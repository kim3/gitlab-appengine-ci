import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from browser import get_browser_driver, BASE_URL


class ExampleTestClass(unittest.TestCase):

    def setUp(self):
        print("BASE_URL", BASE_URL)
        self.driver = get_browser_driver()

    def tearDown(self):
        self.driver.quit()

    def test_page_title(self):
        self.driver.get(BASE_URL)
        print("driver.title", self.driver.title)
        self.assertIn("Gitlab AppEngine CI", self.driver.title)
        elem = self.driver.find_element(By.NAME, "search")
        elem.send_keys("Selenium")
        # elem.send_keys(Keys.RETURN)
        # assert "No results found." not in driver.page_source

    def test_javascript_text(self):
        self.driver.get(BASE_URL)
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located(
            (By.CSS_SELECTOR, 'div#output')))
        elem = self.driver.find_element(By.ID, 'output')
        self.assertIn("JavaScript + gulp too!", elem.text)

if __name__ == "__main__":
    unittest.main()
