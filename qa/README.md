## End To End Tests

All unit tests together
```
python -m unittest discover -s qa
```

A single test can be run with:
```
python qa/test_example.py
````

You can also change the BASE_URL, SELENIUM (hub address), or DRIVER (browser) by passing in environment variables. This will make chrome not run in headless mode so it is visible.
```
DRIVER="chrome" BASE_URL="http://localhost:3000" python -m unittest discover -s qa
DRIVER="chrome" BASE_URL="http://localhost:3000" python qa/test_example.py
```

## Viewing tests using VNC

Install a VNC Viewer such as:
https://chrome.google.com/webstore/detail/vnc%C2%AE-viewer-for-google-ch/iabmpiboiopbgfabjmgeedhcmjenhbla?hl=en

Then connect using:
```
Address: localhost:5900
Password: secret
```