# gitlab-appengine-ci

An example Gitlab workflow demonstrating:
* Unit tests (PyUnit)
* Functional tests (Selenium)
* Build backend/frontend (pip, npm)
* Deployments (gcloud, AppEngine)


## Installation

```
docker-compose build
```


## Usage

```
docker-compose up
```
Then view it at:
https://localhost:3000


## Running tests locally

All tests together
```
python -m unittest discover
```

Unit, functional tests separately:
```
python -m unittest discover -s backend
python -m unittest discover -s qa
```

## Automatic Deployment

Check the .gitlab-ci.yml to see the steps. Set Gitlab Environment Variables as:
* `GAE_KEY`:  Secret key for a user with the required permissions.
* `GAE_PROJECT`:  Name of the target AppEngine application.


## Manual Deployment

Install and build the frontend:

    cd frontend
    npm install
    npm run build

Install and build the backend:

    cd backend
    pip install -t lib -r requirements.txt

Set your Google Cloud project by following the steps at:

    gcloud init

Deploy your project to AppEngine using:

    gcloud app deploy
